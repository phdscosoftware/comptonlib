// Compton.cpp: implementation of the CCompton class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "Compton.h"
#include "StringFile.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif


#define CROSSSECTIONTABLESIZE	80000
#define TABLE_MULT              10000       // CROSSSECTIONTABLESIZE / 8.0 (= log10() range)
#define POINTTOSURFACETABLESIZE	361
#define KLEINNISHINAATABLESIZE	180			// Klein-Nishina cross section table bin size of angle
#define KLEINNISHINAETABLESIZE	3000		// Klein-Nishina cross section table bin size of energy
#define MAXIMUMENERGY			3.0			// in MeV


//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////


CCompton::CCompton()
{
#ifdef COMPTON_ENABLED
	m_strCurCrossSection  = _T(""); // Set to empty to ensure file load on first call to InitCompton()
	m_iCurLongitude	      = -1;	    // Set to illegal value to ensure init on first call to InitCompton()
	m_iCurLatitude		  = -1;     // Set to illegal value to ensure init on first call to InitCompton()

	m_bHistFOM            = FALSE;
	m_bFlipFOM            = FALSE;
	m_iMinFOM             = 0;
	m_iMaxFOM             = 999;

	// The following tables will be initialized by InitCompton and are dependant on
	// the domain values of m_iLatitude and m_iLongitude.
	m_pSinPhiTable        = NULL;
	m_pCosPhiTable        = NULL;
	m_pSinThetaTable      = NULL;
	m_pCosThetaTable      = NULL;
	m_pAreaThetaTable     = NULL;

	// The following tables are created and initialized here only once.
	m_pAcosTable		  = new double[TRIFUNCTIONTABLESIZE];
	m_pAsinTable		  = new double[TRIFUNCTIONTABLESIZE];
	m_pGaussianTable	  = new double[1000];

	// Init the tables: m_pAcosTable and m_pAsinTable domains run from -1 to +1
	double dAngStep = TWOPI / (double)TRIFUNCTIONTABLESIZE;
	double dValStep = 2.0 / (double)(TRIFUNCTIONTABLESIZE - 1);
	double dAngle   = 0.0;
	double dValue   = -1.0;
	for( int i = 0; i < TRIFUNCTIONTABLESIZE; i++, dAngle += dAngStep, dValue += dValStep )
	{
		m_pAcosTable[i] = acos(dValue);
		m_pAsinTable[i] = asin(dValue);
	}
	m_dTableFact = (double)(TRIFUNCTIONTABLESIZE - 1) / 2.0; 

	// Init the gaussian table, x/sigma is between 0 and 5.0
	for( i = 0; i < 1000; i++ )
		m_pGaussianTable[i] = exp(-Square(i/200.0)/2.0);

	// initialize the cross section variables
	m_pPhotonEnergy                   = new double[256];
	m_pCoherentScattering             = new double[256];
	m_pIncoherScattering              = new double[256];
	m_pPhotoElectric                  = new double[256];
	m_pPairInNuclearField             = new double[256];
	m_pPairInElectronField            = new double[256];
	m_pTotalWithCoherentScatt         = new double[256];
	m_pTotalWithoutCoherentScatt      = new double[256];

	// create cross section table
	m_pCoherentScatteringTable        = new double[CROSSSECTIONTABLESIZE];
	m_pIncoherScatteringTable         = new double[CROSSSECTIONTABLESIZE];
	m_pPhotoElectricTable             = new double[CROSSSECTIONTABLESIZE];
	m_pPairInNuclearFieldTable        = new double[CROSSSECTIONTABLESIZE];
	m_pPairInElectronFieldTable       = new double[CROSSSECTIONTABLESIZE];
	m_pTotalWithCoherentScattTable    = new double[CROSSSECTIONTABLESIZE];
	m_pTotalWithoutCoherentScattTable = new double[CROSSSECTIONTABLESIZE];

	// create Klein-Nishina Cross Section table
	m_pKleinNishinaCrossSectionTable  = new double[KLEINNISHINAETABLESIZE * KLEINNISHINAATABLESIZE];

	// Compute the factor NZ which gives the number of electrons per cubic mm for Germanium.  Z (32) is the electrons per atom and
	// N is the atoms per mm3 computed as (Density (5.323e-3 g/mm3) / Atomic Weight (72.64 g/mol) * 6.022e23 (atoms/mol))
	// Compute a factor that is NZ multiplied by the square of the classical electron radius = 2.817940285e-12 mm
	double  dFactor = 3.50416827409e-4,
		    dEngDiv = MAXIMUMENERGY / (double)KLEINNISHINAETABLESIZE,
			dAngDiv = PI / (double)KLEINNISHINAATABLESIZE,
			dEng    = 0.0,
		   *pTable  = m_pKleinNishinaCrossSectionTable;	

	for( int ie = 0; ie < KLEINNISHINAETABLESIZE; ie++, dEng += dEngDiv )
	{
		double a     = dEng / e0,
			   a2    = Square(a),
			   theta = 0.0;

		for( int ia = 0; ia < KLEINNISHINAATABLESIZE; ia++, theta += dAngDiv, pTable++ )
		{
			double costheta  = cos(theta),
				   t         = 1.0 + a * (1.0 - costheta),
				   cos2Plus1 = Square(costheta) + 1.0;

			// the Klein Nishina cross section usually is in the format of micro cross section (cm2)
			// here, we will return the macro cross section, which is in the unit of mm-1
			// macro cross section = micro cross section * number density
			*pTable = (dFactor * cos2Plus1) / (2.0 * Square(t)) * (1.0 + ((a2 * Square(1.0 - costheta)) / (t * cos2Plus1)));
		}
	}
#endif
}

CCompton::~CCompton()
{
#ifdef COMPTON_ENABLED
	DeleteTables();

	delete[] m_pAcosTable;
	delete[] m_pAsinTable;
	delete[] m_pGaussianTable;

	delete[] m_pPhotonEnergy;
	delete[] m_pCoherentScattering;
	delete[] m_pIncoherScattering;
	delete[] m_pPhotoElectric;
	delete[] m_pPairInNuclearField;
	delete[] m_pPairInElectronField;
	delete[] m_pTotalWithCoherentScatt;
	delete[] m_pTotalWithoutCoherentScatt;

	delete[] m_pCoherentScatteringTable;
	delete[] m_pIncoherScatteringTable;
	delete[] m_pPhotoElectricTable;
	delete[] m_pPairInNuclearFieldTable;
	delete[] m_pPairInElectronFieldTable;
	delete[] m_pTotalWithCoherentScattTable;
	delete[] m_pTotalWithoutCoherentScattTable;
	
	delete[] m_pKleinNishinaCrossSectionTable;
#endif
}

BOOL CCompton::InitCompton()
{
	BOOL bChanged = FALSE;

#ifdef COMPTON_ENABLED
	m_dXYUncert2      = Square(m_dXUncertainty) + Square(m_dYUncertainty);
	m_dXYUncertMult   = 1.0;
	m_dZUncert2       = 2.0 * Square(m_dZUncertainty);

	m_dEngUncertSlope = m_dFanoFactor * m_dIonizationEnergy / 1000000.0;
	m_dEngUncertInt   = Square(m_dElectronicNoise / 2350.0 );

	if( (m_iLongitude != m_iCurLongitude) || (m_iLatitude != m_iCurLatitude) )
	{
		bChanged        = TRUE;
		m_iCurLongitude = m_iLongitude;
		m_iCurLatitude  = m_iLatitude;

		DeleteTables();

		m_pSinPhiTable     = new double[m_iLongitude + 1];
		m_pCosPhiTable     = new double[m_iLongitude + 1];
		m_pSinThetaTable   = new double[m_iLatitude];
		m_pCosThetaTable   = new double[m_iLatitude];
		m_pAreaThetaTable  = new double[m_iLatitude];
		
		// initialize the sin and cos tables, phi is [0, TWOPI), and theta is [0, PI]
		double dPhiStep   = TWOPI / (double)m_iLongitude,
			   dThetaStep = PI   /  (double)m_iLatitude,
			   dAngle     = 0.0;

		for( int i = 0; i <= m_iLongitude; i++, dAngle += dPhiStep )
		{
			m_pSinPhiTable[i] = sin( dAngle );
			m_pCosPhiTable[i] = cos( dAngle );
		}

		m_pSinThetaTable[0] = 0.0;
		m_pCosThetaTable[0] = 1.0;
		for( i = 1, dAngle = dThetaStep; i < m_iLatitude; i++, dAngle += dThetaStep )
		{
			m_pSinThetaTable[i]      = sin( dAngle );
			m_pCosThetaTable[i]      = cos( dAngle );
			m_pAreaThetaTable[i - 1] = (m_pCosThetaTable[i - 1] - m_pCosThetaTable[i]) * dPhiStep;
		}
		m_pAreaThetaTable[i - 1] = (m_pCosThetaTable[i - 1] - cos( dAngle )) * dPhiStep;
	}

	if( m_strCurCrossSection != m_strCrossSectionPath ) // File has been changed.
		LoadCrossSectionFile();
#endif
	return bChanged;
}

BOOL CCompton::SequenceReconstruct2Events( PIXELDATA *data, double dTotEng, double *pData, double *pDataMax )
{
#ifdef COMPTON_ENABLED
#if 0
	int iOrder = -1;

	switch( m_e2PixelSequenceMode )
	{
		case SimpleComparison2:
			iOrder = SequenceReconstruct2SimpleComparison( data, dTotEng );
			break;

		case Deterministic2:
			iOrder = SequenceReconstruct2Deterministic( data, dTotEng );
			break;
	}
#endif
	m_bHistFOM = FALSE;

	int iDetOrder = SequenceReconstruct2Deterministic(    data, dTotEng ),
		iScOrder  = SequenceReconstruct2SimpleComparison( data, dTotEng ),
		iOrder    = -1;

	if( iScOrder != iDetOrder )
	{
		if( m_e2PixelSequenceMode == Deterministic2 )
		{
			//m_bHistFOM = TRUE; //!m_bFlipFOM;
			//iOrder = SequenceReconstruct2Deterministic( data, dTotEng );
			iOrder = iDetOrder;
		}
		else if( dTotEng <= 0.400  )
			iOrder = iScOrder;
	}
	else
		iOrder = iScOrder;

	// Swap events if if indicated.
	if( iOrder == 1 )
	{
		PIXELDATA temp = data[0];

		data[0] = data[1]; 
		data[1] = temp;
	}

	if( (iOrder != -1) && ReconstructARing( data, 2, dTotEng, pData, pDataMax ) )
		return TRUE;
	else
#endif
		return FALSE;
}

BOOL CCompton::SequenceReconstruct3Events( PIXELDATA *data, double dTotEng, double *pData, double *pDataMax )
{
#ifdef COMPTON_ENABLED
	int iOrder = -1;

	switch( m_e3PixelSequenceMode )
	{
		case MSD34:
			iOrder = SequenceReconstruct3MSD( data, dTotEng );
			break;

		case Deterministic34:
			iOrder = SequenceReconstruct3Deterministic( data, dTotEng );
			break;
	}

	if( (iOrder != -1) && ReconstructARing( data, 3, dTotEng, pData, pDataMax ) )
		return TRUE;
	else
#endif
		return FALSE;
}

BOOL CCompton::SequenceReconstruct4Events( PIXELDATA *data, double dTotEng, double *pData, double *pDataMax )
{
#ifdef COMPTON_ENABLED
	int iOrder = -1;

	switch( m_e4PixelSequenceMode )
	{
		case MSD34:
			iOrder = SequenceReconstruct4MSD( data, dTotEng );
			break;

		case Deterministic34:
			iOrder = SequenceReconstruct4Deterministic( data, dTotEng );
			break;
	}

	if( (iOrder != -1) && ReconstructARing( data, 4, dTotEng, pData, pDataMax ) )
		return TRUE;
	else
#endif
		return FALSE;
}


BOOL CCompton::IsChargeSharing( PIXELDATA *data, int nPixels )
{
#ifdef COMPTON_ENABLED
	if( !m_bExcludeChrgSharing )
		return FALSE;

	double dPitchLimit = 1.5 * m_dPixelPitch;
	double dDepthLimt  = 4.7 * m_dZUncertainty;

	switch( nPixels )
	{
		case 2:
			if( (fabs(data[0].x - data[1].x) <= dPitchLimit) && 
				(fabs(data[0].y - data[1].y) <= dPitchLimit) &&
				(fabs(data[0].z - data[1].z) < dDepthLimt) )
				return TRUE;
			break;

		case 3:
			if( ( (fabs(data[0].x - data[1].x) <= dPitchLimit) && 
				  (fabs(data[0].y - data[1].y) <= dPitchLimit) &&
				  (fabs(data[0].z - data[1].z) <  dDepthLimt) ) || 
				( (fabs(data[0].x - data[2].x) <= dPitchLimit) && 
				  (fabs(data[0].y - data[2].y) <= dPitchLimit) &&
				  (fabs(data[0].z - data[2].z) <  dDepthLimt) ) ||
				( (fabs(data[1].x - data[2].x) <= dPitchLimit) && 
				  (fabs(data[1].y - data[2].y) <= dPitchLimit) &&
				  (fabs(data[1].z - data[2].z) <  dDepthLimt) ) )
				return TRUE;
			break;

		case 4:
			if( ( (fabs(data[0].x - data[1].x) <= dPitchLimit) && 
				  (fabs(data[0].y - data[1].y) <= dPitchLimit) &&
				  (fabs(data[0].z - data[1].z) <  dDepthLimt) ) || 
				( (fabs(data[0].x - data[2].x) <= dPitchLimit) && 
				  (fabs(data[0].y - data[2].y) <= dPitchLimit) &&
				  (fabs(data[0].z - data[2].z) <  dDepthLimt) ) ||
				( (fabs(data[0].x - data[3].x) <= dPitchLimit) && 
				  (fabs(data[0].y - data[3].y) <= dPitchLimit) &&
				  (fabs(data[0].z - data[3].z) <  dDepthLimt) ) ||
				( (fabs(data[1].x - data[2].x) <= dPitchLimit) && 
				  (fabs(data[1].y - data[2].y) <= dPitchLimit) &&
				  (fabs(data[1].z - data[2].z) <  dDepthLimt) ) ||
				( (fabs(data[1].x - data[3].x) <= dPitchLimit) && 
				  (fabs(data[1].y - data[3].y) <= dPitchLimit) &&
				  (fabs(data[1].z - data[3].z) <  dDepthLimt) ) ||
				( (fabs(data[2].x - data[3].x) <= dPitchLimit) && 
				  (fabs(data[2].y - data[3].y) <= dPitchLimit) &&
				  (fabs(data[2].z - data[3].z) <  dDepthLimt) ) )
				return TRUE;
			break;

		default:
			return FALSE;
	};
#endif
	return FALSE;
}


#ifdef COMPTON_ENABLED
void CCompton::DeleteTables()
{
	if( m_pSinPhiTable     != NULL ) delete[] m_pSinPhiTable;
	if( m_pCosPhiTable     != NULL ) delete[] m_pCosPhiTable;
	if( m_pSinThetaTable   != NULL ) delete[] m_pSinThetaTable;
	if( m_pCosThetaTable   != NULL ) delete[] m_pCosThetaTable;
	if( m_pAreaThetaTable  != NULL ) delete[] m_pAreaThetaTable;
}

void CCompton::LoadCrossSectionFile()
{
	// read the cross section data file
	CStringFile	sFile;
	CString	    szLine;
	int 		iEngCnt = 0;

	memset(m_pPhotonEnergy,              0, 256*sizeof(double));
	memset(m_pCoherentScattering,        0, 256*sizeof(double));
	memset(m_pIncoherScattering,         0, 256*sizeof(double));
	memset(m_pPhotoElectric,             0, 256*sizeof(double));
	memset(m_pPairInNuclearField,        0, 256*sizeof(double));
	memset(m_pPairInElectronField,       0, 256*sizeof(double));
	memset(m_pTotalWithCoherentScatt,    0, 256*sizeof(double));
	memset(m_pTotalWithoutCoherentScatt, 0, 256*sizeof(double));

	if( sFile.Open( m_strCrossSectionPath ) )
	{
		m_strCurCrossSection = m_strCrossSectionPath;

		while( sFile.GetNextLine(szLine) && 
			   (szLine != CString("        (MeV)    (cm2/g)  (cm2/g)  (cm2/g)  (cm2/g)  (cm2/g)  (cm2/g)  (cm2/g)")) );
		
		while( sFile.GetNextLine(szLine) )
		{
			double photonEnergy, 
				   coherentScattering, 
				   incoherScattering, 
			       photoElectric, 
				   pairInNuclearField, 
				   pairInElectronField, 
				   totalWithCoherentScatt, 
				   totalWithoutCoherentScatt;

			szLine = szLine.Mid(7);
			sscanf( szLine, "%Le %Le %Le %Le %Le %Le %Le %Le", &photonEnergy, &coherentScattering, &incoherScattering, &photoElectric, 
													   &pairInNuclearField, &pairInElectronField, &totalWithCoherentScatt, &totalWithoutCoherentScatt);
		
			m_pPhotonEnergy[iEngCnt]              = photonEnergy;
			m_pCoherentScattering[iEngCnt]        = coherentScattering;
			m_pIncoherScattering[iEngCnt]         = incoherScattering;
			m_pPhotoElectric[iEngCnt]             = photoElectric;
			m_pPairInNuclearField[iEngCnt]        = pairInNuclearField;
			m_pPairInElectronField[iEngCnt]       = pairInElectronField;
			m_pTotalWithCoherentScatt[iEngCnt]    = totalWithCoherentScatt;
			m_pTotalWithoutCoherentScatt[iEngCnt] = totalWithoutCoherentScatt;

			iEngCnt++;
		}
		sFile.Close();
	}
	else
		AfxMessageBox("Error Loading Compton Cross Section data file.");

	
	// log10(eng) is from -3 to 5, in CROSSSECTIONTABLESIZE bins
	for( int i = 0; i < CROSSSECTIONTABLESIZE; i++ )
	{
		// linear interporate in log scale
		double x      = i / TABLE_MULT - 3.0;
		double energy = pow(10.0, x);
		double x1, x2, y1, y2;

		for( int j = 0, jNext = 1; jNext < iEngCnt; j++, jNext++ )
		{
			if( (energy >= m_pPhotonEnergy[j]) && (energy < m_pPhotonEnergy[jNext]) )
				break;
		}

		x1 = log10(m_pPhotonEnergy[j]);
		x2 = log10(m_pPhotonEnergy[jNext]);
		
		y1 = log10(m_pCoherentScattering[j]);
		y2 = log10(m_pCoherentScattering[jNext]);
		m_pCoherentScatteringTable[i] = pow(10.0,((x-x1)*y2+(x2-x)*y1)/(x2-x1));

		y1 = log10(m_pIncoherScattering[j]);
		y2 = log10(m_pIncoherScattering[jNext]);
		m_pIncoherScatteringTable[i] = pow(10.0,((x-x1)*y2+(x2-x)*y1)/(x2-x1));
		
		y1 = log10(m_pPhotoElectric[j]);
		y2 = log10(m_pPhotoElectric[jNext]);
		m_pPhotoElectricTable[i] = pow(10.0,((x-x1)*y2+(x2-x)*y1)/(x2-x1));
		
		y1 = log10(m_pPairInNuclearField[j]);
		y2 = log10(m_pPairInNuclearField[jNext]);
		m_pPairInNuclearFieldTable[i] = pow(10.0,((x-x1)*y2+(x2-x)*y1)/(x2-x1));
		
		y1 = log10(m_pPairInElectronField[j]);
		y2 = log10(m_pPairInElectronField[jNext]);
		m_pPairInElectronFieldTable[i] = pow(10.0,((x-x1)*y2+(x2-x)*y1)/(x2-x1));
		
		y1 = log10(m_pTotalWithCoherentScatt[j]);
		y2 = log10(m_pTotalWithCoherentScatt[jNext]);
		m_pTotalWithCoherentScattTable[i] = pow(10.0,((x-x1)*y2+(x2-x)*y1)/(x2-x1));
		
		y1 = log10(m_pTotalWithoutCoherentScatt[j]);
		y2 = log10(m_pTotalWithoutCoherentScatt[jNext]);
		m_pTotalWithoutCoherentScattTable[i] = pow(10.0,((x-x1)*y2+(x2-x)*y1)/(x2-x1));
	}
}


// Compute the angular uncertainty based on calculation
void CCompton::GeometryUncertainty( PIXELDATA *pPixelData, double *sigmatheta, double *sigmaphi )
{
	double dXYUncert2   = m_dXYUncert2 * m_dXYUncertMult;
	double dZUncert2    = m_dZUncert2;
	double dxy2         = Square(pPixelData[1].x - pPixelData[0].x) + Square(pPixelData[1].y - pPixelData[0].y);
	double dz           = pPixelData[1].z - pPixelData[0].z;
	double dz2          = Square(dz);

	double dsigmatheta2 = ((dz2 * dXYUncert2) + (dxy2 * dZUncert2)) / Square(dxy2 + dz2);
	double dphi         = sqrt(dXYUncert2 / dxy2);
	double dtheta       = atan(dz / sqrt(dxy2));

	*sigmatheta         = sqrt( dsigmatheta2 );
	*sigmaphi           = 2.0 * asin( sin(dphi / 2) * cos(dtheta) );
}


/*
void CCompton::FOM2Limits( double dTotEng, double *LIM )
{
	double costheta, theta, dE2;

	costheta = 1.0 - e0 / (dTotEng * 9.0);
    theta    = acos(costheta);	
	LIM[1]   = KleinNishinaCrossSection(theta, dTotEng) / Square((dTotEng * 0.9));

	costheta = 1.0 - e0 / dTotEng;
    theta    = acos(costheta) - 0.5235987756;	// subtract 30 degrees.
	dE2      = e0 * dTotEng / (dTotEng * (1 - cos(theta)) + e0);
	LIM[0]   = KleinNishinaCrossSection(theta, dTotEng) / Square(dE2);
}
*/


void CCompton::FOM2Deterministic( PIXELDATA *data, double dTotEng, double *FOM )
{
	double E1[2]    = {data[0].energy, data[1].energy}, 
		   E2[2]    = {data[1].energy, data[0].energy};
	//double dist     = sqrt( Square(data[0].x - data[1].x) + 
	//	                    Square(data[0].y - data[1].y) + 
	//					    Square(data[0].z - data[1].z) );
	//double dPwrMult = dist * GERMANIUM_DENSITY;
	double costheta;

	for( int i = 0; i < 2; i++ )
	{
		costheta = 1.0 - e0 * E1[i] / (dTotEng * E2[i]);

		if( costheta < -1.0 ) continue;

		double theta  = acos(costheta);	
		//double dPower = GetTotalCrossSection(E2[i]) * dPwrMult; // GetTotalCrossSection includes multiply by -0.1
		//FOM[i] = KleinNishinaCrossSection(theta, dTotEng) * exp(dPower) * GetPhotoelectricCrossSection(E2[i]) / Square(E2[i]);
		FOM[i] = KleinNishinaCrossSection(theta, dTotEng) / Square(E2[i]);
	}
}


double CCompton::FOM3MSD( PIXELDATA *data, double dTotEng, int order )
{
	PIXELDATA ThreeInteractions[3];

	memcpy( ThreeInteractions, data, sizeof(ThreeInteractions) );

	Permutations3Events( ThreeInteractions, order );
 
	double E1  = ThreeInteractions[0].energy;
	double E2  = ThreeInteractions[1].energy;
	double E3  = ThreeInteractions[2].energy;
	double E23 = E2 + E3;

	// MSD
	double costheta1e = 1 - e0 * E1 / (E23 * dTotEng);
	double costheta2e = 1 - e0 * E2 / (E3  * E23);

	// check if the sequence is physically possible
	if( (costheta1e < -1.0) || (costheta2e < -1.0) ) return 0.0;

	double xdiff12      = ThreeInteractions[0].x - ThreeInteractions[1].x;
	double ydiff12      = ThreeInteractions[0].y - ThreeInteractions[1].y;
	double zdiff12      = ThreeInteractions[0].z - ThreeInteractions[1].z;

	double xdiff23      = ThreeInteractions[1].x - ThreeInteractions[2].x;
	double ydiff23      = ThreeInteractions[1].y - ThreeInteractions[2].y;
	double zdiff23      = ThreeInteractions[1].z - ThreeInteractions[2].z;

	double d12          = sqrt(Square(xdiff12) + Square(ydiff12) + Square(zdiff12));
	double d23          = sqrt(Square(xdiff23) + Square(ydiff23) + Square(zdiff23));
	double d1223        = d12 * d23;
	double d121223      = d12 * d1223;
	double costheta2r   = (xdiff12 * xdiff23 + ydiff12 * ydiff23 + zdiff12 * zdiff23) / d1223;
	double d12Coeff     = d12 * costheta2r;
	double d1212Coeff   = d12 * d12Coeff;
	double d2323Coeff   = d23 * d23 * costheta2r;
	double d122323Coeff = d12 * d2323Coeff;

	// use the second scatter angle's uncertainty
	double de2    = EnergyUncertainty(E2);		// Square of the E2 uncertainty.
	double de3    = EnergyUncertainty(E3);		// Square of the E3 uncertainty
	double quad3  = Quad(E3);

	double sigmae2 = e0_2 * (quad3 * de2 + Square(E3 + E23) * Square(E2) * de3) / (quad3 * Quad(E23));

	double sigmar2 = (m_dXYUncert2 * ( Square(d1223      * xdiff12 - d1212Coeff * xdiff23)  + 
		                               Square(d2323Coeff * xdiff12 - d1223      * xdiff23)  + 
								       ((d122323Coeff    * xdiff12 - d121223    * xdiff23)  * 
									    (d23             * xdiff12 - d12Coeff   * xdiff23)) +
									   Square(d1223      * ydiff12 - d1212Coeff * ydiff23)  + 
		                               Square(d2323Coeff * ydiff12 - d1223      * ydiff23)  + 
								       ((d122323Coeff    * ydiff12 - d121223    * ydiff23)  * 
									    (d23             * ydiff12 - d12Coeff   * ydiff23))   )) +
				     (m_dZUncert2  * ( Square(d1223      * zdiff12 - d1212Coeff * zdiff23)  + 
		                               Square(d2323Coeff * zdiff12 - d1223      * zdiff23)  + 
								       ((d122323Coeff    * zdiff12 - d121223    * zdiff23)  * 
									    (d23             * zdiff12 - d12Coeff   * zdiff23))   ));
	sigmar2 /= (Quad(d12) * Quad(d23));

	return (sigmae2 + sigmar2) / Square(costheta2e - costheta2r);
}


double CCompton::FOM3Deterministic( PIXELDATA *data, double dTotEng, int order )
{
	PIXELDATA ThreeInteractions[3];

	memcpy( ThreeInteractions, data, sizeof(ThreeInteractions) );

	Permutations3Events( ThreeInteractions, order );

	double E1  = ThreeInteractions[0].energy;
	double E2  = ThreeInteractions[1].energy;
	double E3  = ThreeInteractions[2].energy;
	double E23 = E2 + E3;

	// MSD
	double costheta1 = 1 - e0 * E1 / (dTotEng * E23);
	double costheta2 = 1 - e0 * E2 / (E23     * E3);

	// check if the sequence is physically possible
	if( (costheta1 < -1.0) || (costheta2 < -1.0) ) return 0.0;

	double xdiff12 = ThreeInteractions[0].x - ThreeInteractions[1].x;
	double ydiff12 = ThreeInteractions[0].y - ThreeInteractions[1].y;
	double zdiff12 = ThreeInteractions[0].z - ThreeInteractions[1].z;

	double xdiff23 = ThreeInteractions[1].x - ThreeInteractions[2].x;
	double ydiff23 = ThreeInteractions[1].y - ThreeInteractions[2].y;
	double zdiff23 = ThreeInteractions[1].z - ThreeInteractions[2].z;

	double d12     = sqrt(Square(xdiff12) + Square(ydiff12) + Square(zdiff12));
	double d23     = sqrt(Square(xdiff23) + Square(ydiff23) + Square(zdiff23));
 
	double theta1  = acos(costheta1);
	double theta2  = acos(costheta2);
	double dPower  = (d12 * GetTotalCrossSection(E23) + d23 * GetTotalCrossSection(E3)) * GERMANIUM_DENSITY; // GetTotalCrossSection includes multiply by -0.1

	return  KleinNishinaCrossSection(theta1, dTotEng) * KleinNishinaCrossSection(theta2, E23) *
		    GetComptonCrossSection(E23) * GetPhotoelectricCrossSection(E3) * exp(dPower) / (Square(E23) * Square(E3));
}


double CCompton::FOM4MSD( PIXELDATA *data, double dTotEng, int order )
{
	PIXELDATA FourInteractions[4];
	
	memcpy( FourInteractions, data, sizeof(FourInteractions) );

	Permutations4Events( FourInteractions, order );

	double E1   = FourInteractions[0].energy;
	double E2   = FourInteractions[1].energy;
	double E3   = FourInteractions[2].energy;
	double E4   = FourInteractions[3].energy;
	double E23  = E2 + E3;
	double E34  = E3 + E4;
	double E234 = E2 + E34;

	// MSD
	double costheta1e = 1 - e0 * E1 / (E234 * dTotEng);
	double costheta2e = 1 - e0 * E2 / (E234 * E34);
	double costheta3e = 1 - e0 * E3 / (E4   * E34);

	// check if the sequence is physically possible
	if( (costheta1e < -1.0) || (costheta2e < -1.0) || (costheta3e < -1.0) ) return 0.0;

	double xdiff12      = FourInteractions[0].x - FourInteractions[1].x;
	double ydiff12      = FourInteractions[0].y - FourInteractions[1].y;
	double zdiff12      = FourInteractions[0].z - FourInteractions[1].z;

	double xdiff23      = FourInteractions[1].x - FourInteractions[2].x;
	double ydiff23      = FourInteractions[1].y - FourInteractions[2].y;
	double zdiff23      = FourInteractions[1].z - FourInteractions[2].z;

	double xdiff34      = FourInteractions[2].x - FourInteractions[3].x;
	double ydiff34      = FourInteractions[2].y - FourInteractions[3].y;
	double zdiff34      = FourInteractions[2].z - FourInteractions[3].z;

	double d12          = sqrt(Square(xdiff12) + Square(ydiff12) + Square(zdiff12));
	double d23          = sqrt(Square(xdiff23) + Square(ydiff23) + Square(zdiff23));
	double d34          = sqrt(Square(xdiff34) + Square(ydiff34) + Square(zdiff34));

	double d1223        = d12 * d23;
	double d121223      = d12 * d1223;
	double costheta2r   = (xdiff12 * xdiff23 + ydiff12 * ydiff23 + zdiff12 * zdiff23) / d1223;
	double d12Coeff     = d12 * costheta2r;
	double d1212Coeff   = d12 * d12Coeff;
	double d2323Coeff   = d23 * d23 * costheta2r;
	double d122323Coeff = d12 * d2323Coeff;

	double d2334        = d23 * d34;
	double d232334      = d23 * d2334;
	double costheta3r   = (xdiff23 * xdiff34 + ydiff23 * ydiff34 + zdiff23 * zdiff34) / d2334;
	double d23Coeff     = d23 * costheta2r;
	double d3434Coeff   = d34 * d34 * costheta2r;
	double d233434Coeff = d23 * d3434Coeff;

	double de2    = EnergyUncertainty(E2);		// Square of the E2 uncertainty.
	double de3    = EnergyUncertainty(E3);		// Square of the E3 uncertainty
	double de4    = EnergyUncertainty(E4);		// Square of the E4 uncertainty
	double quad3  = Quad(E3);
	double quad4  = Quad(E4);

	// use the second scatter angle's uncertainty
	double sigmae2 = e0_2 * (quad3 * de2 + Square(E3 + E23) * Square(E2) * de3) / (quad3 * Quad(E23));

	double sigmar2 = (m_dXYUncert2 * ( Square(d1223      * xdiff12 - d1212Coeff * xdiff23)  + 
		                               Square(d2323Coeff * xdiff12 - d1223      * xdiff23)  + 
								       ((d122323Coeff    * xdiff12 - d121223    * xdiff23)  * 
									    (d23             * xdiff12 - d12Coeff   * xdiff23)) +
									   Square(d1223      * ydiff12 - d1212Coeff * ydiff23)  + 
		                               Square(d2323Coeff * ydiff12 - d1223      * ydiff23)  + 
								       ((d122323Coeff    * ydiff12 - d121223    * ydiff23)  * 
									    (d23             * ydiff12 - d12Coeff   * ydiff23))   )) +
				     (m_dZUncert2  * ( Square(d1223      * zdiff12 - d1212Coeff * zdiff23)  + 
		                               Square(d2323Coeff * zdiff12 - d1223      * zdiff23)  + 
								       ((d122323Coeff    * zdiff12 - d121223    * zdiff23)  * 
									    (d23             * zdiff12 - d12Coeff   * zdiff23))   ));
	sigmar2 /= (Quad(d12) * Quad(d23));

	double FOM = (sigmae2 + sigmar2) / Square(costheta2e - costheta2r);

	// add the third scatter angle's uncertainty
	sigmae2 = e0_2 * (quad4 * de3 + Square(E4 + E34) * Square(E3) * de4) / (quad4 * Quad(E34)); 

	sigmar2 = (m_dXYUncert2 * ( Square(d2334      * xdiff23 - d2323Coeff * xdiff34)  + 
		                        Square(d3434Coeff * xdiff23 - d2334      * xdiff34)  + 
							    ((d233434Coeff    * xdiff23 - d232334    * xdiff34)  * 
							     (d34             * xdiff23 - d23Coeff   * xdiff34)) +
							    Square(d2334      * ydiff23 - d2323Coeff * ydiff34)  + 
		                        Square(d3434Coeff * ydiff23 - d2334      * ydiff34)  + 
							    ((d233434Coeff    * ydiff23 - d232334    * ydiff34)  * 
							     (d34             * ydiff23 - d23Coeff   * ydiff34))   )) +
			  (m_dZUncert2  * ( Square(d2334      * zdiff23 - d2323Coeff * zdiff34)  + 
		                        Square(d3434Coeff * zdiff23 - d2334      * zdiff34)  + 
							    ((d233434Coeff    * zdiff23 - d232334    * zdiff34)  * 
							     (d34             * zdiff23 - d23Coeff   * zdiff34))   ));

	sigmar2 /= (Quad(d23) * Quad(d34));

	FOM += (sigmae2 + sigmar2) / Square(costheta3e - costheta3r);

	return FOM;
}


double CCompton::FOM4Deterministic( PIXELDATA *data, double dTotEng, int order )
{
	PIXELDATA FourInteractions[4];
	
	memcpy( FourInteractions, data, sizeof(FourInteractions) );

	Permutations4Events( FourInteractions, order );

	double E1   = FourInteractions[0].energy;
	double E2   = FourInteractions[1].energy;
	double E3   = FourInteractions[2].energy;
	double E4   = FourInteractions[3].energy;
	double E34  = E3 + E4;
	double E234 = E2 + E34;

	// MSD
	double costheta1 = 1 - e0 * E1 / (E234 * dTotEng);
	double costheta2 = 1 - e0 * E2 / (E234 * E34);
	double costheta3 = 1 - e0 * E3 / (E4   * E34);

	// check if the sequence is physically possible
	if( (costheta1 < -1.0) || (costheta2 < -1.0) || (costheta3 < -1.0) ) return 0.0;

	double xdiff12 = FourInteractions[0].x - FourInteractions[1].x;
	double ydiff12 = FourInteractions[0].y - FourInteractions[1].y;
	double zdiff12 = FourInteractions[0].z - FourInteractions[1].z;

	double xdiff23 = FourInteractions[1].x - FourInteractions[2].x;
	double ydiff23 = FourInteractions[1].y - FourInteractions[2].y;
	double zdiff23 = FourInteractions[1].z - FourInteractions[2].z;

	double xdiff34 = FourInteractions[2].x - FourInteractions[3].x;
	double ydiff34 = FourInteractions[2].y - FourInteractions[3].y;
	double zdiff34 = FourInteractions[2].z - FourInteractions[3].z;

	double d12     = sqrt(Square(xdiff12) + Square(ydiff12) + Square(zdiff12));
	double d23     = sqrt(Square(xdiff23) + Square(ydiff23) + Square(zdiff23));
	double d34     = sqrt(Square(xdiff34) + Square(ydiff34) + Square(zdiff34));

	double theta1  = acos(costheta1);
	double theta2  = acos(costheta2);
	double theta3  = acos(costheta3);

	double dPower  = (d12 * GetTotalCrossSection(E234) + d23 * GetTotalCrossSection(E34) + d34 * GetTotalCrossSection(E4)) * GERMANIUM_DENSITY; // GetTotalCrossSection includes multiply by -0.1

	return  KleinNishinaCrossSection(theta1, dTotEng) * KleinNishinaCrossSection(theta2, E234) * KleinNishinaCrossSection(theta3, E34) *
		    GetComptonCrossSection(E234) * GetPhotoelectricCrossSection(E34) * GetPhotoelectricCrossSection(E4) * exp(dPower) / 
			(Square(E234) * Square(E34) * Square(E4));
}


// the following cross section value is the unit of cm2/g, which is 1 over absorber thickness, or macro cross section divided by density
// when using this cross section (maybe not proper to call it "cross section" here), be careful to multiple the density of the detector
double CCompton::GetTotalCrossSection(double energy)
// energy unit: MeV, CrossSection unit: cm2/g
// return value is the total cross section with coherent scattering times -0.1 (to include the sign flip and divide by 10)
{
	return( -0.1 * m_pTotalWithCoherentScattTable[int((log10(energy)+3.0)*TABLE_MULT)] );  // Coherent scattering???
}

double CCompton::GetComptonCrossSection(double energy)
{
	return m_pIncoherScatteringTable[int((log10(energy)+3.0)*TABLE_MULT)];
}

double CCompton::GetPhotoelectricCrossSection(double energy)
{
	return m_pPhotoElectricTable[int((log10(energy)+3.0)*TABLE_MULT)];
}

double CCompton::KleinNishinaCrossSection( double theta, double dTotEng )
{
	int nIndex = int(dTotEng / MAXIMUMENERGY * KLEINNISHINAETABLESIZE) * KLEINNISHINAATABLESIZE + 
		         int(theta / PI * KLEINNISHINAATABLESIZE);

	if( nIndex >= KLEINNISHINAETABLESIZE * KLEINNISHINAATABLESIZE )
		return 0;
	else
		return m_pKleinNishinaCrossSectionTable[nIndex];
}


int CCompton::SequenceReconstruct2SimpleComparison( PIXELDATA *data, double dTotEng )
{
	BOOL   bSwap = FALSE;
	double costheta;

	if( dTotEng > 0.400 )
	{
		if(data[0].energy < data[1].energy)
			bSwap = TRUE;
	}
	else
	{
		if(data[0].energy > data[1].energy)
			bSwap = TRUE;
	}

	if( bSwap )
	{
		costheta = 1.0 - (e0 * data[1].energy) / (dTotEng * data[0].energy);

		if( costheta < -1.0 ) return -1;

		return 1;
	}
	else
	{
		costheta = 1.0 - (e0 * data[0].energy) / (dTotEng * data[1].energy);

		if( costheta < -1.0 ) return -1;

		return 0;
	}
}

int CCompton::SequenceReconstruct2Deterministic( PIXELDATA *data, double dTotEng )
{
	double FOM[2] = {0.0, 0.0};

	FOM2Deterministic( data, dTotEng, FOM );

	if( FOM[1] > FOM[0] )
	{
		if( m_bHistFOM )
		{
			int iVal = (int)(data[0].energy * 500.0);
			//int iVal = (int)(FOM[1] * 1000000.0);
			//if( iVal > 999 ) iVal = 999;
			//m_iFomHist[iVal]++;;

			if( (iVal < m_iMinFOM) || (iVal > m_iMaxFOM) )
			{
				if( !m_bFlipFOM )
				{
					m_iFomHist[iVal]++;
				}
				return -1;
			}
			else if( m_bFlipFOM )
			{
				m_iFomHist[iVal]++;
			}
		}

		return 1;
	}
	else if( FOM[0] != 0.0 )
	{
		if( m_bHistFOM )
		{
			int iVal = (int)(data[1].energy * 500.0);
			//int iVal = (int)(FOM[0] * 1000000.0);
			//if( iVal > 999 ) iVal = 999;			    
			//m_iFomHist[iVal]++;

			if( (iVal < m_iMinFOM) || (iVal > m_iMaxFOM) )
			{
				if( !m_bFlipFOM )
				{
					m_iFomHist[iVal]++;
				}
				return -1;
			}
			else if( m_bFlipFOM )
			{
				m_iFomHist[iVal]++;
			}
		}

		return 0;
	}
	else
		return -1;
}


// if the sequence is not changed, return true, else return false
int CCompton::SequenceReconstruct3MSD( PIXELDATA *data, double dTotEng )
{
	double FOM[6];
	int	   nmax = 0;

	for( int i = 0; i < 6; i++ )
	{ 
		FOM[i] = FOM3MSD( data, dTotEng, i );

		if( FOM[i] > FOM[nmax] )
			nmax = i;
	}

	if( FOM[nmax] == 0.0 ) return -1;

	Permutations3Events( data, nmax );
	return nmax;
}

int CCompton::SequenceReconstruct3Deterministic( PIXELDATA *data, double dTotEng )
{
	double FOM[6];
	int	   nmax = 0;

	for( int i = 0; i < 6; i++ )
	{ 
		FOM[i] = FOM3Deterministic( data, dTotEng, i );

		if( FOM[i] > FOM[nmax] )
			nmax = i;
	}

	if( FOM[nmax] == 0.0 ) return -1;

	Permutations3Events( data, nmax );
	return nmax;
}


int CCompton::SequenceReconstruct4MSD( PIXELDATA *data, double dTotEng )
{
	double FOM[24];
	int	   nmax = 0;

	for( int i = 0; i < 24; i++ )
	{ 
		FOM[i] = FOM4MSD( data, dTotEng, i );

		if( FOM[i] > FOM[nmax] )
			nmax = i;
	}

	if( FOM[nmax] == 0.0 ) return -1;

	Permutations4Events( data, nmax );
	return nmax;
}

int CCompton::SequenceReconstruct4Deterministic( PIXELDATA *data, double dTotEng )
{
	double FOM[24];
	int	   nmax = 0;

	for( int i = 0; i < 24; i++ )
	{ 
		FOM[i] = FOM4Deterministic( data, dTotEng, i );

		if( FOM[i] > FOM[nmax] )
			nmax = i;
	}

	if( FOM[nmax] == 0.0 ) return -1;

	Permutations4Events( data, nmax );
	return nmax;
}


void CCompton::Permutations3Events( PIXELDATA *data, int order )
{
	PIXELDATA temp;

	switch (order)
	{
		case 0:
		default:
			break;

		case 1:
			temp    = data[1];
			data[1] = data[2];
			data[2] = temp;
			break;

		case 2:
			temp    = data[0];
			data[0] = data[1];
			data[1] = temp;
			break;

		case 3:
			temp    = data[0];
			data[0] = data[1];
			data[1] = data[2];
			data[2] = temp;
			break;

		case 4:
			temp    = data[0];
			data[0] = data[2];
			data[2] = data[1];
			data[1] = temp;
			break;

		case 5:
			temp    = data[0];
			data[0] = data[2];
			data[2] = temp;
			break;
	};
}


void CCompton::Permutations4Events( PIXELDATA *data, int order )
{
	PIXELDATA temp;

	switch (order)
	{
		case 0:
		default:
			break;

		case 1:
			temp    = data[1];
			data[1] = data[2];
			data[2] = temp;
			break;

		case 2:
			temp    = data[0];
			data[0] = data[1];
			data[1] = temp;
			break;

		case 3:
			temp    = data[0];
			data[0] = data[1];
			data[1] = data[2];
			data[2] = temp;
			break;

		case 4:
			temp    = data[0];
			data[0] = data[2];
			data[2] = data[1];
			data[1] = temp;
			break;

		case 5:
			temp    = data[0];
			data[0] = data[2];
			data[2] = temp;
			break;

		case 6:
			temp    = data[2];
			data[2] = data[3];
			data[3] = temp;
			break;

		case 7:
			temp    = data[1];
			data[1] = data[3];
			data[3] = data[2];
			data[2] = temp;
			break;

		case 8:
			temp    = data[0];
			data[0] = data[1];
			data[1] = temp;
			temp    = data[3];
			data[3] = data[2];
			data[2] = temp;
			break;

		case 9:
			temp    = data[0];
			data[0] = data[1];
			data[1] = data[3];
			data[3] = data[2];
			data[2] = temp;
			break;

		case 10:
			temp    = data[0];
			data[0] = data[3];
			data[3] = data[2];
			data[2] = data[1];
			data[1] = temp;
			break;

		case 11:
			temp    = data[0];
			data[0] = data[3];
			data[3] = data[2];
			data[2] = temp;
			break;

		case 12:
			temp    = data[1];
			data[1] = data[3];
			data[3] = temp;
			break;

		case 13:
			temp    = data[1];
			data[1] = data[2];
			data[2] = data[3];
			data[3] = temp;
			break;

		case 14:
			temp    = data[0];
			data[0] = data[3];
			data[3] = data[1];
			data[1] = temp;
			break;

		case 15:
			temp    = data[0];
			data[0] = data[3];
			data[3] = data[1];
			data[1] = data[2];
			data[2] = temp;
			break;

		case 16:
			temp    = data[0];
			data[0] = data[2];
			data[2] = data[3];
			data[3] = data[1];
			data[1] = temp;
			break;

		case 17:
			temp    = data[0];
			data[0] = data[2];
			data[2] = temp;
			temp    = data[1];
			data[1] = data[3];
			data[3] = temp;
			break;

		case 18:
			temp    = data[0];
			data[0] = data[3];
			data[3] = temp;
			break;

		case 19:
			temp    = data[0];
			data[0] = data[3];
			data[3] = temp;
			temp    = data[1];
			data[1] = data[2];
			data[2] = temp;
			break;

		case 20:
			temp    = data[0];
			data[0] = data[1];
			data[1] = data[3];
			data[3] = temp;
			break;

		case 21:
			temp    = data[0];
			data[0] = data[1];
			data[1] = data[2];
			data[2] = data[3];
			data[3] = temp;
			break;

		case 22:
			temp    = data[0];
			data[0] = data[2];
			data[2] = data[1];
			data[1] = data[3];
			data[3] = temp;
			break;

		case 23:
			temp    = data[0];
			data[0] = data[2];
			data[2] = data[3];
			data[3] = temp;
			break;	
	};
}


BOOL CCompton::ReconstructARing( PIXELDATA *pPixelData, int nPixels, double dTotEng, double *pData, double *pDataMax )
{
	double dXdiff = pPixelData[0].x - pPixelData[1].x;
	double dYdiff = pPixelData[0].y - pPixelData[1].y;
	double dZdiff = pPixelData[0].z - pPixelData[1].z;

	// the cone axis direction
	double theta1 = PIOVER2 - atan(dZdiff / sqrt(Square(dXdiff) + Square(dYdiff)));	// theta is the angle between the direction and z axis
	double phi1   = atan2(dYdiff, dXdiff);	// must use atan2 here because phi ranges from -pi to pi

	if( phi1 < 0 ) phi1 += TWOPI;

	// the scatter angle
	double dE1        = pPixelData[0].energy;
	double dRemEng    = dTotEng - dE1;
	double dEngProd   = dTotEng * dRemEng;
	double costheta1e = 1 - e0 * dE1 / dEngProd;

	if( fabs(costheta1e) >= 1 )
		return FALSE;

	double alpha = acos(costheta1e);

	// angular uncertainty caused by energy uncertainty
	double de1 = EnergyUncertainty(dE1), 
		   de2 = 0.0;

	for( int i = 1; i < nPixels; i++ )
	{
		de2 += EnergyUncertainty( pPixelData[i].energy );
	}

	double sigmae2 = e0_2 * (Quad(dRemEng) * de1 + Square(Square(dE1) + 2*dE1*dRemEng) * de2) / (Quad(dEngProd) * (1.0 - Square(costheta1e)));

	// angular uncertainty caused by position uncertainty
	double sigmaalphaPtheta;
	double sigmaalphaPphi;
	GeometryUncertainty( pPixelData, &sigmaalphaPtheta, &sigmaalphaPphi );

	double sigmaalphaPdiff = sigmaalphaPphi - sigmaalphaPtheta;

	double costheta1 = cos(theta1);
	double sintheta1 = sin(theta1);
	double cosphi1   = cos(phi1);
	double sinphi1   = sin(phi1);

	double cosphi2, sinphi2;
	double costheta2, sintheta2;

	double alphap;
	double cosalphap;
	double dWeight = 1.0 / (TWOPI * sin(alpha) * 4119.0); // divided by 4119.0 is to normalize the sum of each event to be equal to 1.;

	// used by calculated angular uncertainty
	double sinphi2mphi1;
	double sinphi2mphi1sq;
	double sinbetasq;
	double sigmaalpha;
	int	   nGaussianIndex1;
	int    nGaussianIndex2;

	double dTemp;
	double dsintheta1cosphi1 = sintheta1 * cosphi1;
	double dsintheta1sinphi1 = sintheta1 * sinphi1;
	double dFactsintheta2;

	double dDataMax  = 0.0;

	// add a ring
	for( i = 0; i < m_iLongitude; i++ )
	{
		cosphi2        = m_pCosPhiTable[i];
		sinphi2        = m_pSinPhiTable[i];
		sinphi2mphi1   = (sinphi2 * cosphi1) - (cosphi2 * sinphi1); // sin(phi2 - phi1);
		sinphi2mphi1sq = Square( sinphi2mphi1 );
		dFactsintheta2 = (dsintheta1cosphi1 * cosphi2) + (dsintheta1sinphi1 * sinphi2);

		for( int j = 0; j < m_iLatitude; j++, pData++ )
		{
			//////////////////////////////////////////////////////////////////
			// THE FOLLOWING MATH FUNCTIONS ARE FORBIDEN IN THIS LOOP
			// sin, cos, tan, asin, acos, atan, atan2, exp, log, log10, pow, etc
			// AND PLEASE USE AS FEW AS POSSIBLE OF THE FOLLOWING FUNCTION
			// sqrt
			// for the consideration of speed
			//////////////////////////////////////////////////////////////////

			costheta2 = m_pCosThetaTable[j];
			sintheta2 = m_pSinThetaTable[j];

			//cosalphap = sintheta1 * sintheta2 * (cosphi1*cosphi2 + sinphi1*sinphi2) + (costheta1 * costheta2);
			cosalphap = (dFactsintheta2 * sintheta2) + (costheta1 * costheta2);

			alphap = m_pAcosTable[int((cosalphap + 1.0) * m_dTableFact)];
			if( alphap == 0 )
			{
				sinbetasq = 0.5;	// in case alphap is equal to zero, beta is set to 45 degree
			}
			else
			{
				sinbetasq = Square(sintheta2) * sinphi2mphi1sq / (1 - Square(cosalphap));

				if( sinbetasq > 1.0 ) sinbetasq = 1.0;
			}

			// this is a heavy work
			dTemp      = sigmaalphaPtheta + sigmaalphaPdiff * sinbetasq;
			sigmaalpha = sqrt(sigmae2 + Square(dTemp));	// this approximation is good, however, it will fail when the half 
			                                            // cone angle is small or sigmaalphaPtheta/sigmaalphaPPhi is large

			// add gaussian shaped weight to the points according to the angle distance from the cone
			dTemp           = 200.0 / sigmaalpha;
			nGaussianIndex1 = int(fabs(alphap - alpha) * dTemp);
			nGaussianIndex2 = int(fabs(alphap + alpha) * dTemp);

			if(nGaussianIndex1 >= 1000) nGaussianIndex1 = 999;
			if(nGaussianIndex2 >= 1000) nGaussianIndex2 = 999;

			*pData += dWeight * (m_pGaussianTable[nGaussianIndex1] + m_pGaussianTable[nGaussianIndex2]) / sigmaalpha;

			if( *pData > dDataMax )
				dDataMax = *pData;	// Record the max value. 
		}
	}

	*pDataMax = dDataMax;
	return TRUE;
}

#endif