// Compton.h: interface for the CCompton class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_COMPTON_H__1971C4CC_91CE_489D_88EE_48AA490D83A6__INCLUDED_)
#define AFX_COMPTON_H__1971C4CC_91CE_489D_88EE_48AA490D83A6__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "math.h"

#ifndef PI
	#define PI						3.1415926535897932384626433832795
	#define TWOPI					6.283185307179586476925286766559
	#define PIOVER2					1.5707963267948966192313216916398
#endif

#define e0							0.511		// MeV
#define e0_2						0.261121	// MeV

#define GERMANIUM_DENSITY			5.323		// g/cm3

#define TRIFUNCTIONTABLESIZE		10000

#define DEFAULT_LATITUDE			180
#define DEFAULT_LONGITUDE			180
#define DEFAULT_X_UNCERTAINTY		"0.5"
#define DEFAULT_Y_UNCERTAINTY		"0.5"
#define DEFAULT_Z_UNCERTAINTY		"0.5"
#define DEFAULT_IONIZATION_ENG		"3.0"
#define DEFAULT_FANO_FACTOR			"0.12"
#define DEFAULT_ELECTRONIC_NOISE	"1.5"
#define CROSS_SECT_FILE				"CrossSection_Ge.txt"

// Types of two pixel sequence reconstruction.
enum	 
{
	Disabled2,
	SimpleComparison2,
	Deterministic2
};

// Types of three and four pixel sequence reconstruction.
enum	 
{
	Disabled34,
	MSD34,
	Deterministic34
};

#pragma pack(push, 1)

typedef struct _PIXELDATA
{
	double x;		// x, y, z in mm
	double y;
	double z;
	double energy;	// energy in MeV
} PIXELDATA, *P_PIXELDATA;

#pragma pack(pop)

inline double Square(double input) { return input*input;     }
inline double Quad(  double input) { return pow(input, 4.0); }

class CCompton  
{
public:
	CCompton();
	virtual ~CCompton();

	BOOL     InitCompton();
	BOOL     SequenceReconstruct2Events( PIXELDATA *data, double dTotEng, double *pData, double *pDataMax );
	BOOL     SequenceReconstruct3Events( PIXELDATA *data, double dTotEng, double *pData, double *pDataMax );
	BOOL     SequenceReconstruct4Events( PIXELDATA *data, double dTotEng, double *pData, double *pDataMax );

	BOOL     IsChargeSharing( PIXELDATA *data, int nPixels );

	int		 m_iLatitude;
	int		 m_iLongitude;
	int		 m_iCurLatitude;
	int		 m_iCurLongitude;

	int		 m_e2PixelSequenceMode;
	int	   	 m_e3PixelSequenceMode;
	int		 m_e4PixelSequenceMode;

	double	 m_dPixelPitch;
	double	 m_dXUncertainty;
	double	 m_dYUncertainty;
	double	 m_dZUncertainty;
	double	 m_dXYUncert2;
	double   m_dXYUncertMult;
	double   m_dZUncert2;
	double	 m_dElectronicNoise;
	double	 m_dFanoFactor;
	double	 m_dIonizationEnergy;
	double   m_dEngUncertSlope;
	double   m_dEngUncertInt;
	BOOL     m_bExcludeChrgSharing;
	CString  m_strCrossSectionPath;
	CString  m_strCurCrossSection;

	double	*m_pSinPhiTable;
	double	*m_pCosPhiTable;
	double	*m_pSinThetaTable;
	double	*m_pCosThetaTable;
	double  *m_pAreaThetaTable;

	double  *m_pAcosTable;
	double  *m_pAsinTable;
	double   m_dTableFact;

	int      m_iFomHist[1000];
	int      m_iMinFOM;
	int      m_iMaxFOM;
	BOOL     m_bHistFOM;
	BOOL     m_bFlipFOM;

protected:
	double  *m_pGaussianTable;

	// cross section data
	double	*m_pPhotonEnergy;
	double	*m_pCoherentScattering;
	double	*m_pIncoherScattering;
	double	*m_pPhotoElectric;
	double	*m_pPairInNuclearField;
	double	*m_pPairInElectronField;
	double	*m_pTotalWithCoherentScatt;
	double	*m_pTotalWithoutCoherentScatt;

	double	*m_pCoherentScatteringTable;
	double	*m_pIncoherScatteringTable;
	double	*m_pPhotoElectricTable;
	double	*m_pPairInNuclearFieldTable;
	double	*m_pPairInElectronFieldTable;
	double	*m_pTotalWithCoherentScattTable;
	double	*m_pTotalWithoutCoherentScattTable;
	double  *m_pKleinNishinaCrossSectionTable;

	void    DeleteTables();
	void    LoadCrossSectionFile();
	void    GeometryUncertainty( PIXELDATA *pPixelData, double *sigmatheta, double *sigmaphi );

	double  GetPhotoelectricCrossSection( double energy );
	double  GetComptonCrossSection(       double energy );
	double  GetTotalCrossSection(         double energy );
	double	KleinNishinaCrossSection( double theta, double E0 );

	void    FOM2Deterministic( PIXELDATA *data, double dTotEng, double *FOM );
	double  FOM3MSD(           PIXELDATA *data, double dTotEng, int order );
	double  FOM3Deterministic( PIXELDATA *data, double dTotEng, int order );
	double  FOM4MSD(           PIXELDATA *data, double dTotEng, int order );
	double  FOM4Deterministic( PIXELDATA *data, double dTotEng, int order );

	void    Permutations4Events( PIXELDATA *data, int order );
	void    Permutations3Events( PIXELDATA *data, int order );

	int		SequenceReconstruct2SimpleComparison( PIXELDATA *data, double dTotEng );
	int		SequenceReconstruct2Deterministic(    PIXELDATA *data, double dTotEng );
	int		SequenceReconstruct3MSD(              PIXELDATA *data, double dTotEng );
	int		SequenceReconstruct3Deterministic(    PIXELDATA *data, double dTotEng );
	int		SequenceReconstruct4MSD(              PIXELDATA *data, double dTotEng );
	int		SequenceReconstruct4Deterministic(    PIXELDATA *data, double dTotEng );

	BOOL    ReconstructARing( PIXELDATA *pPixelData, int nPixels, double dTotEng, double *pData, double *pDataMax );

	// energy in unit of MeV, return value is the standard deviation
	inline double  EnergyUncertainty( double energy ) { return(m_dEngUncertSlope * energy + m_dEngUncertInt); }	
};

#endif // !defined(AFX_COMPTON_H__1971C4CC_91CE_489D_88EE_48AA490D83A6__INCLUDED_)
